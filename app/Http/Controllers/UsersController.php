<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $user)
    {
        /*SELECT * 
        FROM visits
        LEFT JOIN states ON visits.city = states.id
        WHERE visits.user =7
        LIMIT 0 , 30*/

        //gets latest 100 by the user
        $visits = \DB::table('visits')
            ->leftJoin('states', 'visits.city', '=', 'states.id')
            ->select('states.city', 'states.state')
            ->where('visits.user', '=', $user)
            ->orderBy('visits.id', 'desc')
            ->groupBy('states.id')
            ->take(100)
            ->get();
        //$cities = Visits::where('user', '=', $user)->lists('name');
        echo json_encode($visits);


    }

    public function addVisit(Request $request, $user)
    {

        $city = \DB::table('states')
            ->select('id')
            ->where('state', '=', $request->input('state', ''))
            ->where('city', '=', $request->input('city', ''))
            ->get();
        if(count($city) == 0){
            echo json_encode(Array('error' => 'City doesnt exist'));
            return;
        }

        \DB::table('visits')->insert(
            ['user' => $user, 'city' => $city[0]->id]
         );

        echo json_encode(Array('success' => 'true'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
