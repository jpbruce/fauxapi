<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\States;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request, $state)
    {

        $cities = States::where('state', '=', $state)->lists('city');
        echo json_encode($cities);
    }

    //get cities around other cities
    public function cities(Request $request, $state, $city)
    {
        $radius = $request->input('radius', 5);
        if(is_numeric($radius)){
            $radius = intval($radius);
        }else{
            $radius = 5;
        }

        $state = States::where('state', '=', $state)->where('city', '=', $city)->first();
        //make sure there was a result
        if(empty($state)){
            echo json_encode(Array('error' => 'city doesnt exist'));
            return;
        }

        //3959 = miles; 6371 = kilos
       //$results = States::raw("
        $results = \DB::select( \DB::raw("
        SELECT city, state, ( 3959 * acos ( cos ( radians(".$state->latitude.") ) * cos( radians( latitude ) )
            * cos( radians( longitude ) - radians(".$state->longitude.") )
            + sin ( radians(".$state->latitude.") )
            * sin( radians( latitude ) )
          )) AS distance
        FROM states
        HAVING distance < ".$radius."
        ORDER BY distance ASC LIMIT 10"));

        echo json_encode($results);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
