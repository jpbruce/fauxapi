<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


//Route::get('states.cities.city', 'StatesController@cities');
Route::get('states/{state}/cities', [
    'as' => 'state.cities',
    'uses' => 'StatesController@index'
]);


Route::get('states/{state}/cities/{city}', [
    'as' => 'city.of.state',
    'uses' => 'StatesController@cities'
]);
  
Route::post('users/{user}/visits', [
    'as' => 'visit.of.user',
    'uses' => 'UsersController@addVisit'
]);

Route::get('users/{user}/visits', [
    'as' => 'visits.user',
    'uses' => 'UsersController@index'
]);